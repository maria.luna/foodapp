import 'react-native-gesture-handler';
import React, { Component } from 'react'

import RecetaLayout from "./receta/containers/section2"
import DetailLayout from "./receta/containers/detail"
import LoginLayout from "./receta/containers/login"
import RegisterLayout from "./receta/containers/register"
import VideoLayout from "./receta/containers/video"

import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'

const Stack = createStackNavigator();

class App extends Component{


    render(){
        return(
          <NavigationContainer>
            <Stack.Navigator screenOptions={{headerShown: false}}>
              <Stack.Screen name="Home" component={RecetaLayout} />
              <Stack.Screen name="Detail" component={DetailLayout} />
              <Stack.Screen name="Login" component={LoginLayout} />
              <Stack.Screen name="Register" component={RegisterLayout} />
              <Stack.Screen name="Video" component={VideoLayout} />
            </Stack.Navigator>
          </NavigationContainer>
        )
    }
}

export default App;
