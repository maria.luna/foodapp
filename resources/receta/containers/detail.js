
import React from 'react'
import { Header, Button } from 'react-native-elements'
import {Image, View, Text, StyleSheet, ScrollView, TextInput } from 'react-native'
import Icon1 from 'react-native-vector-icons/AntDesign';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';

// import Icon from 'react-native-vector-icons/dist/FontAwesome';
import data from '../../data/receta.json'
import MyVideo from './video';

function Detail({ route, navigation }){

  const [value, onChangeText] = React.useState('');
 
  const { itemId } = route.params;
  const item = data.items.find(x=> x.ID === itemId);
  
  let array = [1,2,3,4,5,6]

  let array2 = [1,2,3,4,5]

  onPressQuestion = (value) =>{
    console.log("RESPUESTA : ", value)
  }

  

  return (
    <View style={style.container}>
      <Header
        leftComponent={{ icon: 'arrow-back', color: '#fff',  onPress: ( ()=> navigation.navigate('Home') )  }}
        containerStyle={{
          backgroundColor: '#FCA133'
        }}
      />
      <View style={style.body}>
          <ScrollView >
            <Image source={{uri: item.IMAGE}} style={style.imageRecipe} ></Image>
            <View style={style.section}>
              <Text style={style.title} >{item.TITLE.toUpperCase()}</Text>
              <Text style={style.indicadorPlato}> 250 cal - 20g Proteinas - 30g Carboidratos - 5g Grasas</Text>
              <Text style={style.detail}>{item.DESCRIPTION.substring(0,150)}.</Text>
              <View>
                <Text style={{fontWeight:'bold', fontSize: 18, paddingBottom: 10}} >Ingredientes</Text>
                
                { array.map((item, i) =>
                  <View key={i} style={{flexDirection:'row', alignContent:'center' , alignItems: 'center' , marginTop: 5 }}>
                    <Icon2 name='silverware-fork-knife' size={14}  /> 
                  <Text style={{fontSize: 15}} > 1/4 kg Ingrediente {i+1}</Text>
                </View>
                )}
                
              </View>

              <View style={{flexDirection:'row', alignContent:'center' , alignSelf:'flex-end' , alignItems: 'center' , marginTop: 5 , paddingTop: 10, marginRight: 10 }}>
                <Icon1 name='hearto' size={20} style={{marginRight:10}}  /> 
                <Text   style={{alignContent:'center' ,  alignItems: 'center', fontSize: 18 }} >{Math.floor(Math.random() * 100)} k</Text>
              </View>

              <View style={style.preparacion}>
                <Text style={{textTransform:'capitalize' , fontWeight:'bold' , fontSize: 18 }} >preparación</Text>
                {
                  array2.map((item1, i) => 
                  <View style={style.pasoItem} key={i}>
                    <Text style={style.titlePaso}>Paso {i+1}</Text>
                    <Text style={style.descripcionPaso}>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour.</Text>
                    <Image source={{uri: item.IMAGE}} style={style.imageRecipe} ></Image>
                  </View>
                  )
                }
              </View> 


              <View style={{ paddingTop: 20 }}>
                <View style={{ flexDirection: 'column'}} >
                  <Text style={{textTransform:'capitalize' , fontWeight:'500' , fontSize: 14 }} >¿Satisfecho con la receta?</Text>
                  <View style={{ flexDirection:'row' , justifyContent:'space-between' , width: '50%', paddingTop: 10, paddingLeft: 15, paddingBottom: 15 }} >
                    <Button
                      type="outline"
                      onPress={()=> this.onPressQuestion(true)}
                      title="Si"
                      buttonStyle={{ borderColor:"#000", width: 50 , height: 30, padding: 0 , borderWidth: 0.3 }}
                      titleStyle={{ fontSize: 14, color: '#000' }}
                    />
                    <Button
                      onPress={()=> this.onPressQuestion(false)}
                      type="outline"
                      title="No"
                      buttonStyle={{ borderColor:"#000", width: 50 , height: 30, padding: 0 , borderWidth: 0.3 }}
                      titleStyle={{ fontSize: 14, color: '#000' }}
                    />
                  </View>
                </View>


                <View style={{ flexDirection: 'column'}} >
                  <Text style={{textTransform:'capitalize' , fontWeight:'500' , fontSize: 14 }} >Dejanos un comentario</Text>
                  <TextInput 
                    multiline
                    numberOfLines={5}
                    onChangeText={text => onChangeText(text.substring(0, 100))}
                    value={value}
                    style={{ height:100, borderColor: '#000', borderWidth: 0.5 , borderRadius: 5 , width: '80%', marginTop: 15, marginLeft: 15, marginBottom: 10, paddingTop: 15 , paddingLeft: 10, paddingRight: 10  }} />
                </View>
              </View> 
            </View>

            <View style={{ width:'100%', height: 100 }}>

            </View>
          </ScrollView>
      </View>
    </View>

  );
}

const style = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'white'
  },
  header: {
    height: 105,
    flexDirection: 'row',
    // alignItems: ''
  },
  body: {
    flex: 1,
    flexDirection: 'column'
  },
  section:{
    padding: 15
  },
  logo: {
    marginTop: 30,
    // marginLeft: 1100,
    width: 70,
    height: 70,
    borderRadius: 35,
  },
  title: {
    fontSize: 20,
    fontWeight: '500',
    marginBottom: 10
  },
  indicadorPlato:{
    fontSize: 12,
    fontWeight: '300',
    marginBottom: 10
  },
  like: {
    fontSize: 20,
    textAlign: 'center',
    fontWeight: '500',
    paddingTop: 20,
    paddingBottom: 10
  },
  detail: {
    fontSize: 14,
    fontWeight: 'bold',
    textAlign: 'justify',
    fontFamily: 'Arial',
    paddingBottom: 10
  },
  imageRecipe: {
    height: 250,
    width:'100%',
  },
  preparacion:{
    paddingTop: 10,
    paddingBottom: 10
  },
  pasoItem:{
    paddingTop: 10
  },
  titlePaso:{
    fontWeight:'500'
  },
  descripcionPaso:{
    paddingTop: 5,
    paddingBottom: 10
  }
})


export default Detail;
