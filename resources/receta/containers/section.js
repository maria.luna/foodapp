
import React from 'react'
import { Header } from 'react-native-elements'
import {Image, View, Text, StyleSheet, ScrollView, TouchableHighlight } from 'react-native'
import data from '../../data/receta.json'

function Section(props){
  const { navigation } = props

  return(
    <View style={style.container}>
      {/* <View style={style.header}>
        <Image source={{uri: 'https://us1desafoodapp.s3.amazonaws.com/logo.png' }} style={style.logo} ></Image>
      </View> */}
      <Header
        leftComponent={<Image source={{uri: 'https://us1desafoodapp.s3.amazonaws.com/logo.png' }} style={style.logo} ></Image>}
        // centerComponent={{ text: 'FoodApp', style: { color: '#fff' } }}
        // rightComponent={{ icon: 'search', color: '#fff' }}
      />
      <View style={style.body}>
        <Text style={style.title}>Tendencias</Text>
        <View style={style.list}>
          <ScrollView>

            {
              data.items.map((item, i) => {
                if(i % 2 === 0){
                  const nextItem = i + 1;
                  return(
                    <View style={style.rowRecipe} key={i}>
                      <View style={style.itemRecipe}>
                        <TouchableHighlight
                            style={style.imgItem}
                            onPress={() => navigation.navigate('Detail', {
                              itemId: data.items[i].ID
                            })}>
                            <Image source={{uri: data.items[i].IMAGE }}  style={style.imageRecipe} ></Image>
                        </TouchableHighlight>
                        <Text style={style.titleRecipe} >{data.items[i].TITLE}</Text>
                      </View> 
                      <View style={style.itemRecipe}>
                        <TouchableHighlight
                            style={style.imgItem}
                            onPress={() => navigation.navigate('Detail', {
                              itemId: data.items[nextItem].ID
                            })}>
                            <Image source={{uri: data.items[nextItem].IMAGE }}  style={style.imageRecipe} ></Image>
                        </TouchableHighlight>
                        <Text style={style.titleRecipe} >{data.items[nextItem].TITLE}</Text>
                      </View> 
                    </View>
                  )
                }
              })
            }


          </ScrollView>
        </View>
      </View>
    </View>
  )
}

const style = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'white'
  },
  header: {
    height: 105,
    borderBottomWidth: 0.3,
    borderBottomColor: 'black',
    // justifyContent: 'center',
  },
  logo: {
    // marginTop: 30,
    width: 30,
    height: 30,
    borderRadius: 35,
    // alignSelf: 'center'
  },
  title: {
    fontSize: 20,
    textAlign: 'center',
    fontWeight: '500',
    paddingTop: 20,
    paddingBottom: 10
  },
  rowRecipe: {
    flex: 1,
    flexDirection: 'row',
    // alignItems: 'stretch',
    // flexWrap: 'wrap',
    // backgroundColor: 'black'
  },
  itemRecipe: {
    width:'50%',
    marginTop: 10,
    // marginBottom: 10
  },
  imgItem: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  titleRecipe:{
    marginTop: 5,
    marginLeft: 5
  },
  imageRecipe: {
    height: 120,
    width:'99%',
    borderRadius: 5,
  }
})

export default Section;
