
import React from 'react'
import { Header,FormLabel, FormInput, FormValidationMessage } from 'react-native-elements'
import {Picker, Text, StyleSheet, View, TextInput, Button, Image} from 'react-native';


export default class Register extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
          autenticado: false,
          nombres: "",
          apellidos: "",
          correo: "",
          contrasena: "",
          intereses: [{
            item: "Postres dulces",
            active: true
          },
          {
            item: "Tragos cortos",
            active: false
          },
          {
            item: "Vegano",
            active: true
          }]
        };
    }

    componentDidMount(){
        
    }

    handleChange = (e, inputName)=>{
        this.setState({ [inputName]: e.nativeEvent.text})
    }

    registrarse(){
  

        const { navigation } = this.props;

        navigation.navigate('Home')

        /*fetch("https://r6h6m26gfl.execute-api.us-east-2.amazonaws.com/desa/usuario/registrar")
        .then(response => response.json())
        .then((responseJson)=> {
          this.setState({
           autenticado: true
          })
        })
        .catch(error=>console.log(error)) */
    }

    render(){
        
        const { navigation } = this.props
        
        return(
            <View style={style.container}>
                <Header
                    leftComponent={<Image source={{uri: 'https://us1desafoodapp.s3.amazonaws.com/logo.png' }} style={style.logo} ></Image>}
                    centerComponent={{ text: 'FoodApp', style: { color: '#fff' } }}
                    rightComponent={{ icon: 'search', color: '#fff' }}
                />
                <Text style={style.title}> Registrate </Text>
                <View>
                    <TextInput placeholder="Nombres"  style={style.inputTest} onChange={(text) => { this.handleChange(text, 'nombres')}} value={this.state.nombres} />
                    <TextInput placeholder="Apellidos"  style={style.inputTest} onChange={(text) => { this.handleChange(text, 'apellidos')}} value={this.state.apellidos} />
                    <TextInput placeholder="Correo electrónico"  style={style.inputTest} onChange={(text) => { this.handleChange(text, 'correo')}} value={this.state.correo} />
                    <TextInput secureTextEntry={true} placeholder="Contraseña" style={style.inputTest} onChange={(text) => { this.handleChange(text, 'contrasena')}} value={this.state.contrasena} />
                    <Button title="Registrarse" onPress={this.registrarse.bind(this)} color="#841584"></Button>
                    <Button title="Ver Video" onPress={() => navigation.navigate('Video')} color="#841584"></Button>
                </View>
            </View>
        )
       
    }
}

const style = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'white'
  },
  inputTest: {
    height: 40, borderColor: 'gray', borderWidth: 1 
  },
  header: {
    height: 105,
    borderBottomWidth: 0.3,
    borderBottomColor: 'black',
    // justifyContent: 'center',
  },
  logo: {
    // marginTop: 30,
    width: 30,
    height: 30,
    borderRadius: 35,
    // alignSelf: 'center'
  },
  title: {
    fontSize: 20,
    textAlign: 'center',
    fontWeight: '500',
    paddingTop: 20,
    paddingBottom: 10
  },
  rowRecipe: {
    flex: 1,
    flexDirection: 'row',
    // alignItems: 'stretch',
    // flexWrap: 'wrap',
    // backgroundColor: 'black'
  },
  itemRecipe: {
    width:'50%',
    marginTop: 10,
    // marginBottom: 10
  },
  imgItem: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  titleRecipe:{
    marginTop: 5,
    marginLeft: 5
  },
  imageRecipe: {
    height: 120,
    width:'99%',
    borderRadius: 5,
  }
})

