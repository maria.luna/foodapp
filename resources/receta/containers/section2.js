
import React from 'react'
import { Header } from 'react-native-elements'
import {Image, View, Text, StyleSheet, ScrollView, TouchableHighlight } from 'react-native'

export default class Section extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
          loading: true,
          dataItems:[]
        };
    }

    async componentDidMount(){
        fetch("https://r6h6m26gfl.execute-api.us-east-2.amazonaws.com/desa/receta/listar")
        .then(response => response.json())
        .then((responseJson)=> {
          this.setState({
           loading: false,
           dataItems: JSON.parse(responseJson.body)
          })
        })
        .catch(error=>console.log(error)) 
    }

    render(){
        
        const { navigation } = this.props
        
        if(!this.state.loading){
            return(
                <View style={style.container}>
                <Header
                    leftComponent={<Image source={{uri: 'https://us1desafoodapp.s3.amazonaws.com/logo.png' }} style={style.logo} ></Image>}
                    containerStyle={{
                      backgroundColor: '#FCA133'
                    }}
                />
                <View style={style.body}>
                    <Text style={style.title}>Tendencias</Text>
                    <View style={style.list}>
                    <ScrollView>

                        {
                        this.state.dataItems.map((item, i) => {
                            if(i % 2 === 0){
                            const nextItem = i + 1;
                            return(
                                <View style={style.rowRecipe} key={i}>
                                <View style={style.itemRecipe}>
                                    <TouchableHighlight
                                        style={style.imgItem}
                                        onPress={() => navigation.navigate('Detail', {
                                        itemId: this.state.dataItems[i].ID
                                        })}>
                                        <Image source={{uri: this.state.dataItems[i].IMAGE }}  style={style.imageRecipe} ></Image>
                                    </TouchableHighlight>
                                    <Text style={style.titleRecipe} >{this.state.dataItems[i].TITLE}</Text>
                                </View> 
                                <View style={style.itemRecipe}>
                                    <TouchableHighlight
                                        style={style.imgItem}
                                        onPress={() => navigation.navigate('Detail', {
                                        itemId: this.state.dataItems[nextItem].ID
                                        })}>
                                        <Image source={{uri: this.state.dataItems[nextItem].IMAGE }}  style={style.imageRecipe} ></Image>
                                    </TouchableHighlight>
                                    <Text style={style.titleRecipe} >{this.state.dataItems[nextItem].TITLE}</Text>
                                </View> 
                                </View>
                            )
                            }
                        })
                        }


                    </ScrollView>
                    </View>
                </View>
                </View>
            )
        } else {
            return(<><Text style={style.titleRecipe} > cargando... </Text></>)
        }
    }
}

const style = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'white'
  },
  header: {
    height: 105,
    borderBottomWidth: 0.3,
    borderBottomColor: 'black',
    // justifyContent: 'center',
  },
  logo: {
    // marginTop: 30,
    width: 30,
    height: 30,
    borderRadius: 35,
    // alignSelf: 'center'
  },
  title: {
    fontSize: 20,
    textAlign: 'center',
    fontWeight: '500',
    paddingTop: 20,
    paddingBottom: 10
  },
  rowRecipe: {
    flex: 1,
    flexDirection: 'row',
    // alignItems: 'stretch',
    // flexWrap: 'wrap',
    // backgroundColor: 'black'
  },
  itemRecipe: {
    width:'50%',
    marginTop: 10,
    // marginBottom: 10
  },
  imgItem: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  titleRecipe:{
    marginTop: 5,
    marginLeft: 5
  },
  imageRecipe: {
    height: 120,
    width:'99%',
    borderRadius: 5,
  }
})

